# How does a Robot’s Social Credibility Affect Safety Performance

The SocCred project aims to assess how potentially conflicting safety and social behaviour requirements on a robot may be managed.
This repository contains experimental design, procedure, and code for the initial experiment.

Results have been [published](https://link.springer.com/chapter/10.1007%2F978-3-030-35888-4_69), also please see the [paper's sources](https://gitlab.com/robothouse/rh-papers/credibility-safety-experiment).

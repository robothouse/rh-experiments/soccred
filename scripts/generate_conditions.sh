#!/bin/bash

set -o nounset

A=AN
B=VN
REPEATS=15
OUTPUT=$1

printf "$A\n$B\n%.0s" $(seq 1 $REPEATS) | shuf | cat -n |  sed 's/\t/,/g' | sed 's/[[:space:]]//g' > $OUTPUT

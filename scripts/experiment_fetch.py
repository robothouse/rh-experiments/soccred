#!/usr/bin/python

import sys
import rospy
import time

from math import hypot

from std_msgs.msg import String
from geometry_msgs.msg import PoseWithCovarianceStamped
from safety_security.msg import PopupMsg
from actionlib_msgs.msg import GoalStatus
from move_base_msgs.msg import MoveBaseGoal, MoveBaseActionResult

from time import sleep
from uh_robots.util import get_parameter
from uh_core.db.access import Actuators, Sensors
from uh_robots.util import ExtendedBehaviour

from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

from uh_robots.fetch_util import PointHeadClient
from uh_robots.fetch_util import MoveBaseClient
from uh_robots.fetch_util import FollowTrajectoryClient
from uh_robots.fetch_util import GripperClient
from uh_robots.fetch_util import GraspingClient
from uh_robots.fetch_util import SceneClient
from uh_robots.fetch_util import PerceptionClient
from uh_robots.fetch_util import ArmClient

class SensorMonitor(object):

    def __init__(self, targets):
      self.sensors = Sensors()
      self.targets = targets
      self.times = None
      self.aborted = False

    def execute(self):
      current_time = time.time()
      self.times = {sid:current_time for sid in self.targets.keys()}

      while not self.aborted and not len(self.targets) == 0:
        self.targets = {sid:status for (sid,status) in self.targets.items() if not self.check_log(sid,status)}
        time.sleep(.1)

    def abort(self):
      self.aborted = True
      for sid, desired_status in self.targets.items():
        sensor = self.sensors.getSensor(sid)
        name = sensor['name']
        status = sensor['status']
        rospy.loginfo("[SEMON] Sensor %s (%s) timed out waiting for '%s' after '%ss' with status '%s'.", sid, name, desired_status, (time.time() - self.times[sid]), status)

    def check_log(self, sid, desired_status):
      sensor = self.sensors.getSensor(sid)
      name = sensor['name']
      status = sensor['status']

      if status == desired_status:
        rospy.loginfo("[SEMON] Sensor %s (%s) reached '%s' after '%ss'.", sid, name, desired_status, (time.time() - self.times[sid]))
        return True
      else:
        return False

class TopicMonitor(object):

    def __init__(self, topic, target="RELAX/MOTORS"):
      self.time = None
      self.aborted = False
      self.data = None
      self.topic = topic
      self.target = target
      rospy.Subscriber(self.topic, String, self._callback)

    def _callback(self, msgdata):
      self.data = msgdata.data

    def execute(self):
      self.time = time.time()
      while not self.aborted and not self.check_log():
        time.sleep(.1)

    def abort(self):
      self.aborted = True
      rospy.loginfo("[TOMON] Timed out waiting for '%s' on topic '%s' after '%ss' with data '%s'.", self.target, self.topic, (time.time() - self.time), self.data)

    def check_log(self):
      if self.data == self.target:
        rospy.loginfo("[TOMON] Received '%s' on topic '%s' after '%ss'.", self.data, self.topic, (time.time() - self.time))
        return True
      else:
        return False

class InsufficientInformationException(Exception):
  pass

class StoppedException(Exception):
  pass

class ExperimentControl(object):

  def __init__(self):

    self.head = PointHeadClient()
    self.gripper = GripperClient()
    self.base = MoveBaseClient()
    self.torso = FollowTrajectoryClient("torso", ["torso_lift_joint"])
    self.grasp = GraspingClient()
    self.scene = SceneClient()
    self.percept = PerceptionClient()
    self.arm = ArmClient()
    self.sound = SoundClient()

    self.state = None
    self.signal = ""
    self.acted = None
    self.position = None
    self.experiment_id = None
    self.condition = None

    rospy.Subscriber('/safety_security/cmd', String, self._callback)
    rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self._posecb)
    self.notify_pub = rospy.Publisher('/safety_security/notify', PopupMsg, queue_size=1)
    self.cmd_pub = rospy.Publisher('/safety_security/cmd', String, queue_size=1)
    self.actuators = Actuators()
    self.sound_path = rospy.get_param("~sound_path", "/tmp")
    rospy.loginfo("[SCRIPT] Ready.")

  def _callback(self, msgdata):
    cmd = msgdata.data
    if "stop" in self.signal and not "init" in self.signal:
      rospy.logwarn("[SCRIPT] Ignored signal '%s' while in state '%s'", cmd, self.signal)
    else:
      rospy.loginfo("[SCRIPT] Received signal, changing state from '%s' -> '%s'", self.signal, cmd)
      self.signal = cmd

  def _posecb(self, msgdata):
    self.position = msgdata.pose.pose.position
    rospy.logdebug("[SCRIPT] Updated position to '%s,%s'.", self.position.x, self.position.y)


  def say_item(self, item, wait=2):
    #utterance = get_parameter("/script_server/utterances/" + item)
    utterance = self.sound_path + "/" + item + ".ogg"
    rospy.loginfo("<<say>> '%s' (%s)", item, utterance)

    #self.sound.say(utterance)
    self.sound.playWave(self.sound_path + "/" + item + ".ogg")
    time.sleep(wait)

  def prepare(self, item, phrase=None, only_phrase=False, wait=30, position="standby"):
    assert (not "stop" in self.signal)
    if not self.acted is None and not only_phrase:
      self.say_item(self.acted + "/" + self.condition)

    if not phrase is None:
      self.say_item(phrase + "/" + self.condition)

    rospy.loginfo("[SCRIPT] EXPERIMENT PHASE ID '%s', COND '%s': PUZZLE/INSPECT(%s).", self.experiment_id, self.condition, item)
    self.check_result(self.head.look_at("straight"), "head")
    self.check_result(self.arm.tuck_position(retries=3), "arm")
    self.check_result(self.base.goto(position), "base")
    time.sleep(wait)

  def check_result(self, result, component="robot"):
    if not result is None:
      if type(result) == bool and not result:
        rospy.logwarn("[SCRIPT] Error moving '%s', aborting.", component)
        raise StoppedException()
      elif type(result) == MoveBaseActionResult and not result.status == GoalStatus.SUCCEEDED:
        rospy.logwarn("[SCRIPT] Error moving '%s: '%s', aborting.", component, result.status)
        raise StoppedException()
    assert (not "stop" in self.signal)

  def display(self, message, title="NOTIFICATION", note="", label="Dismiss"):
    msg = PopupMsg()
    msg.message = message
    msg.visible = True
    msg.title = title
    msg.note = note
    msg.label = label

    self.notify_pub.publish(msg)

  def init_wait(self):
    self.await_signal(["init"])

    if not rospy.has_param("/safety_security/experiment_id"):
      rospy.logerr("[SCRIPT] Parameter '/safety_security/experiment_id' not present, aborting.")
      raise InsufficientInformationException()
    else:
      self.experiment_id = rospy.get_param('/safety_security/experiment_id')

    if not rospy.has_param("/safety_security/experiment_condition"):
      rospy.logerr("[SCRIPT] Parameter '/safety_security/experiment_condition' not present, aborting.")
      raise InsufficientInformationException()
    else:
      self.condition = rospy.get_param('/safety_security/experiment_condition')

    rospy.set_param('/safety_security/experiment_trials/' + self.experiment_id + '/condition', self.condition)
    self.set_state("INIT")

    #set up plugs
    self.actuators.setActuator(672, 100)
    self.actuators.setActuator(673, 0)
    self.actuators.setActuator(750, 100)
    self.actuators.setActuator(751, 0)
    self.actuators.setActuator(752, 100)
    self.actuators.setActuator(753, 0)
    self.actuators.setActuator(754, 0)
    self.actuators.setActuator(755, 100)

    self.head.look_at("straight")
    self.check_result(self.arm.tuck_position(retries=3))
    self.check_result(self.base.goto("intro"), "base")

  def greet_wait(self):
    self.await_signal()
    self.set_state("GREET")

    self.check_result(self.base.goto("greet" + "/" + self.condition), "base")
    self.say_item("greeting/" + self.condition)

    #self.display(utterance, "EXPERIMENT READY", "Press <strong>Okay</strong> when ready", "Okay")
    #self.sss.move("arm_right", "hello") #TODO decide

    self.await_signal()

  def set_state(self, cur_state):
    rospy.loginfo("[SCRIPT] EXPERIMENT ID '%s', COND '%s' ENTER PHASE: '%s' -> '%s'", self.experiment_id, self.condition, self.state, cur_state)
    rospy.set_param('/safety_security/experiment_trials/' + self.experiment_id + '/state', cur_state)
    self.state = cur_state

  def interrupt_person(self, item, monitor=None, via=None, monitor_class=SensorMonitor):
    self.set_state("INTERRUPT/INITIATED(%s)" % item)

    if via is None:
      self.base.goto("notify" + "/" + self.condition)
    else:
      self.move_via("notify" + "/" + self.condition, via)

    self.set_state("INTERRUPT/ARRIVED(%s)" % item)

    gaze_target = rospy.get_param("/script_server/head/interrupt_fetch/" + self.condition)[0]
    self.head.look_at(gaze_target, duration=1.0)
    self.say_item("interrupt/" + self.condition)
    self.say_item(item + "/" + self.condition)
    self.set_state("INTERRUPT/INFORMED(%s)" % item)

    thread = None
    if not monitor is None:
      thread = ExtendedBehaviour(monitor_class, monitor)

    self.acted = self.await_signal()
    self.set_state("INTERRUPT/DISMISSED(%s)" % item)

    if not thread is None and thread.isAlive():
      thread.stop()

  def inspect_a(self):
    item="oven"
    self.prepare(item, phrase="greet_dismiss", only_phrase=True)
    self.search("inspect/living_1")
    self.base.goto("prepare/kitchen")
    self.search("inspect/kitchen")
    self.base.goto("after/kitchen")
    self.interrupt_person(item, via=["exit/kitchen"])

  def inspect_b(self):
    item = "plugs"
    self.prepare(item)
    self.move_via("inspect/living_2", ["waypoint_a", "waypoint_b"], True)
    self.search()
    self.base.goto("prepare/kitchen")
    self.search("inspect/kitchen")
    self.base.goto("after/kitchen")
    self.interrupt_person(item,
                          monitor={672: "Off",
                           750: "Off",
                           752: "Off",
                           755: "Off",
                           },
                          via=["exit/kitchen"])
    self.actuators.setActuator(672, 0)
    self.actuators.setActuator(673, 0)
    self.actuators.setActuator(750, 0)
    self.actuators.setActuator(751, 0)
    self.actuators.setActuator(752, 0)
    self.actuators.setActuator(753, 0)
    self.actuators.setActuator(754, 0)
    self.actuators.setActuator(755, 0)


  def inspect_c(self):
    item = "plugs_2"
    self.prepare(item)
    self.search("inspect/living_1")
    self.actuators.setActuator(752, 100)
    self.base.goto("prepare/kitchen")
    self.search("inspect/kitchen")
    self.base.goto("after/kitchen")
    self.interrupt_person(item, monitor={752: "Off"}, via=["exit/kitchen"])

  def inspect_d(self):
    item = "pepper"
    self.prepare(item)
    self.search("inspect/living_1")
    self.search("inspect/bedroom")
    self.interrupt_person(item, monitor="/safety_security/pepper", monitor_class=TopicMonitor)

  def search(self, target=None):
    assert (not "stop" in self.signal)
    if not target is None:
      self.base.goto(target)

    self.head.look_at([ 1.0, -0.5, 1.0 ], duration=1.0)
    self.head.look_at([ 1.0, -0.5, 0.0 ], duration=1.0)
    self.head.look_at([ 1.0, -0.5, 2.0 ], duration=1.0)
    self.head.look_at([ 1.0, 0.5, 2.0 ], duration=1.0)
    self.head.look_at([ 1.0, 0.5, 1.0 ], duration=1.0)
    self.head.look_at([ 1.0, 0.5, 0.0 ], duration=1.0)
    self.head.look_at("straight", duration=2.0)

    assert (not "stop" in self.signal)

  def await_signal(self, sig=["ignored", "acted"]):
    rospy.loginfo("[SCRIPT] Awaiting signal '%s'...", sig)
    while not rospy.is_shutdown():
      if "stop" in self.signal:
          raise StoppedException()
      elif self.signal == "":
        pass
      else:
        for si in sig:
          if si in self.signal:
            rospy.loginfo("[SCRIPT] Accepted signal '%s'.", self.signal)
            self.signal = "";
            return si

      time.sleep(.1)

    if rospy.is_shutdown():
      raise StoppedException()

  def move_via(self, target, waypoints, conditions=False, distance=.6):
    for wp in waypoints:
      if conditions:
        wp = wp + "/" + self.condition
      wp_x, wp_y, wp_phi = get_parameter("/script_server/base/", wp)
      self.base.goto(wp, wait=False)

      dist = hypot(wp_x - self.position.x, wp_y - self.position.y)
      while dist > distance:
        time.sleep(.02)
        dist = hypot(wp_x - self.position.x, wp_y - self.position.y)

    self.base.goto(target)

  def execute(self):
    msg = String()
    try:
      self.init_wait()
      self.greet_wait()
      self.inspect_a()
      self.inspect_b()
      self.inspect_c()
      self.inspect_d()
      self.prepare("standby", phrase="standby", position="intro", wait=0)

      self.set_state("COMPLETED")
      msg.data ="stop/script"
      self.cmd_pub.publish(msg)
    except (AssertionError, StoppedException, InsufficientInformationException, Exception):
      self.signal = ""
      if not self.experiment_id is None:
        rospy.logwarn("[SCRIPT] Aborting experiment id '%s', condition '%s'", self.experiment_id, self.condition)
        self.set_state("ABORTED(%s)" % self.state)
        msg.data = "abort/script"
        self.cmd_pub.publish(msg)
      else:
        rospy.logwarn("[SCRIPT] No experiment active, ignoring abort.")

if __name__ == "__main__":
  rospy.init_node("safety_security_experiment_flow")
  script = ExperimentControl()
  while not rospy.is_shutdown():
    script.execute()

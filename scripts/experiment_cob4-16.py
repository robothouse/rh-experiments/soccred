#!/usr/bin/python

import sys
import rospy
import time

from math import hypot

from simple_script_server import *
from std_msgs.msg import String
from geometry_msgs.msg import PoseWithCovarianceStamped
from safety_security.msg import PopupMsg
from actionlib_msgs.msg import GoalStatus

from time import sleep
from uh_robots.util import get_parameter
from uh_core.db.access import Actuators, Sensors
from uh_robots.util import ExtendedBehaviour

class SensorMonitor(object):

    def __init__(self, targets):
      self.sensors = Sensors()
      self.targets = targets
      self.times = None
      self.aborted = False

    def execute(self):
      current_time = time.time()
      self.times = {sid:current_time for sid in self.targets.keys()}

      while not self.aborted and not len(self.targets) == 0:
        self.targets = {sid:status for (sid,status) in self.targets.items() if not self.check_log(sid,status)}
        time.sleep(.1)

    def abort(self):
      self.aborted = True
      for sid, desired_status in self.targets.items():
        sensor = self.sensors.getSensor(sid)
        name = sensor['name']
        status = sensor['status']
        rospy.loginfo("Sensor %s (%s) timed out waiting for '%s' after '%ss' with status '%s'.", sid, name, desired_status, (time.time() - self.times[sid]), status)

    def check_log(self, sid, desired_status):
      sensor = self.sensors.getSensor(sid)
      name = sensor['name']
      status = sensor['status']

      if status == desired_status:
        rospy.loginfo("Sensor %s (%s) reached '%s' after '%ss'.", sid, name, desired_status, (time.time() - self.times[sid]))
        return True
      else:
        return False

class TopicMonitor(object):

    def __init__(self, topic, target="RELAX/MOTORS"):
      self.time = None
      self.aborted = False
      self.data = None
      self.topic = topic
      self.target = target
      rospy.Subscriber(self.topic, String, self._callback)

    def _callback(self, msgdata):
      self.data = msgdata.data

    def execute(self):
      self.time = time.time()
      while not self.aborted and not self.check_log():
        time.sleep(.1)

    def abort(self):
      self.aborted = True
      rospy.loginfo("Timed out waiting for '%s' on topic '%s' after '%ss' with data '%s'.", self.target, self.topic, (time.time() - self.time), self.data)

    def check_log(self):
      if self.data == self.target:
        rospy.loginfo("Received '%s' on topic '%s' after '%ss'.", self.data, self.topic, (time.time() - self.time))
        return True
      else:
        return False

class InsufficientInformationException(Exception):
  pass

class StoppedException(Exception):
  pass

class ExperimentControl(object):

  def __init__(self):
    self.sss=simple_script_server()
    self.signal = ""
    self.position = None
    self.experiment_id = None
    self.condition = None

    rospy.loginfo("Subscribing to /safety_security/cmd")
    rospy.Subscriber('/safety_security/cmd', String, self._callback)
    rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self._posecb)
    self.pub = rospy.Publisher('/safety_security/notify', PopupMsg, queue_size=1)
    self.actuators = Actuators()

  def _callback(self, msgdata):
    cmd = msgdata.data
    if "stop" in self.signal:
      rospy.logwarn("Ignored signal '%s' while in state '%s'", cmd, self.signal)
    else:
      rospy.loginfo("Received signal, changing state from '%s' -> '%s'", self.signal, cmd)
      self.signal = cmd

  def _posecb(self, msgdata):
    self.position = msgdata.pose.pose.position
    rospy.logdebug("Updated position to '%s,%s'.", self.position.x, self.position.y)


  def prepare(self, item, phrase="dismiss", wait=2):
    assert (not "stop" in self.signal)
    if not phrase is None:
      utterance = get_parameter("/script_server/utterances/" + phrase + "/", self.condition)
      self.sss.say("sound", [ utterance ])
      time.sleep(wait)

    rospy.loginfo("EXPERIMENT PHASE ID '%s', COND '%s': PUZZLE/INSPECT(%s).", self.experiment_id, self.condition, item)
    self.check_result(self.sss.move("head", "front"), "head")
    self.check_result(self.sss.move("arm_right", "side"), "arm")
    self.check_result(self.sss.move("base", "standby"), "base")



  def check_result(self, handle, component="robot"):
    state = handle.get_state()
    if not state == GoalStatus.SUCCEEDED:
      rospy.logwarn("Error moving '%s': '%s', aborting.", component, state)
      raise StoppedException()
    assert (not "stop" in self.signal)

  def display(self, message, title="NOTIFICATION", note="", label="Dismiss"):
    msg = PopupMsg()
    msg.message = message
    msg.visible = True
    msg.title = title
    msg.note = note
    msg.label = label

    self.pub.publish(msg)

  def init_wait(self):
    self.await_signal("init")

    if not rospy.has_param("/safety_security/experiment_id"):
      rospy.logerr("parameter '/safety_security/experiment_id' not present, aborting.")
      raise InsufficientInformationException()
    else:
      self.experiment_id = rospy.get_param('/safety_security/experiment_id')

    if not rospy.has_param("/safety_security/experiment_condition"):
      rospy.logerr("parameter '/safety_security/experiment_condition' not present, aborting.")
      raise InsufficientInformationException()
    else:
      self.condition = rospy.get_param('/safety_security/experiment_condition')

    rospy.set_param('/safety_security/experiment_trials/' + self.experiment_id + '/condition', self.condition)
    self.set_state("INIT")

    #set up plugs
    self.actuators.setActuator(672, 100)
    self.actuators.setActuator(673, 0)
    self.actuators.setActuator(750, 100)
    self.actuators.setActuator(751, 0)
    self.actuators.setActuator(752, 100)
    self.actuators.setActuator(753, 0)
    self.actuators.setActuator(754, 0)
    self.actuators.setActuator(755, 100)

    self.sss.move("head", "front")
    self.sss.move("arm_right", "side")
    self.check_result(self.sss.move("base", "intro"), "base")

  def greet_wait(self):
    self.await_signal()
    self.set_state("GREET")

    self.check_result(self.sss.move("base", "greet" + "/" + self.condition), "base")
    utterance = get_parameter("/script_server/utterances/greeting/", self.condition)
    self.sss.say("sound", [ utterance ], False)
    self.display(utterance, "EXPERIMENT READY", "Press <strong>Okay</strong> when ready", "Okay")
    self.sss.move("arm_right", "hello")

    self.await_signal()

  def set_state(self, cur_state):
    rospy.loginfo("EXPERIMENT PHASE ID '%s', COND '%s': %s.", self.experiment_id, self.condition, cur_state)
    rospy.set_param('/safety_security/experiment_trials/' + self.experiment_id + '/state', cur_state)

  def interrupt_person(self, item, monitor=None, monitor_class=SensorMonitor):
    int_utterance = get_parameter("/script_server/utterances/interrupt/", self.condition)
    utterance = get_parameter("/script_server/utterances/" + item + "/", self.condition)
    self.set_state("INTERRUPT/INITIATED(%s)" % item)

    self.sss.move("base", "notify" + "/" + self.condition)
    self.set_state("INTERRUPT/ARRIVED(%s)" % item)

    self.sss.move("head", "interrupt/" + self.condition, False)
    self.sss.say("sound", [ int_utterance ])
    time.sleep(2)
    self.display(utterance)
    self.sss.say("sound", [ utterance ])
    self.set_state("INTERRUPT/INFORMED(%s)" % item)

    thread = None
    if not monitor is None:
      thread = ExtendedBehaviour(monitor_class, monitor)

    self.await_signal()
    self.set_state("INTERRUPT/DISMISSED(%s)." % item)

    if not thread is None and thread.isAlive():
      thread.stop()

  def inspect_a(self):
    item="oven"
    self.prepare(item, "greet_dismiss")
    self.search("inspect/living_1")
    self.sss.move("base", "prepare/kitchen")
    self.search("inspect/kitchen")
    self.interrupt_person(item)

  def inspect_b(self):
    item = "plugs"
    self.prepare(item)
    self.move_via("inspect/living_2", ["waypoint_a", "waypoint_b"])
    self.search()
    self.sss.move("base", "prepare/kitchen")
    self.search("inspect/kitchen")
    self.interrupt_person(item,
                          {672: "Off",
                           750: "Off",
                           752: "Off",
                           755: "Off",
                           })
    self.actuators.setActuator(672, 0)
    self.actuators.setActuator(673, 0)
    self.actuators.setActuator(750, 0)
    self.actuators.setActuator(751, 0)
    self.actuators.setActuator(752, 0)
    self.actuators.setActuator(753, 0)
    self.actuators.setActuator(754, 0)
    self.actuators.setActuator(755, 0)


  def inspect_c(self):
    item = "plugs_2"
    self.prepare(item)
    self.search("inspect/living_1")
    self.actuators.setActuator(752, 100)
    self.sss.move("base", "prepare/kitchen")
    self.search("inspect/kitchen")
    self.interrupt_person(item, {752: "Off"})

  def inspect_d(self):
    item = "pepper"
    self.prepare(item)
    self.search("inspect/living_1")
    self.search("inspect/bedroom")
    self.interrupt_person(item, "/safety_security/pepper", TopicMonitor)

  def search(self, target=None):
    assert (not "stop" in self.signal)
    if not target is None:
      self.sss.move("base", target)
    #self.sss.move("head", "front")
    #self.sss.move("head", "search/left")
    self.sss.move("sensorring", "right")
    #self.sss.move("head", "search/left_down")
    self.sss.move("sensorring", "front")
    #self.sss.move("head", "search/right_down")
    self.sss.move("sensorring", "left")
    #self.sss.move("head", "search/right")
    self.sss.move("sensorring", "front", False)
    #self.sss.move("head", "front")
    assert (not "stop" in self.signal)

  def await_signal(self, sig="dismiss"):
    rospy.loginfo("Awaiting signal '%s'...", sig)
    while not rospy.is_shutdown():
      if sig in self.signal:
        break
      elif self.signal == "":
        pass
      elif "stop" in self.signal:
        raise StoppedException()
      else:
        rospy.logwarn("Unexpected signal '%s', ignoring...", self.signal)
        self.signal = "";
      time.sleep(.5)

    if rospy.is_shutdown():
      raise StoppedException()

    rospy.loginfo("Accepted signal '%s'.", self.signal)
    self.signal = "";
    return True

  def move_via(self, target, waypoints, distance=.5):
    for wp in waypoints:
      wp = wp + "/" + self.condition
      wp_x, wp_y, wp_phi = get_parameter("/script_server/base/", wp)
      self.sss.move("base", wp, False)

      dist = hypot(wp_x - self.position.x, wp_y - self.position.y)
      while dist > distance:
        time.sleep(.05)
        dist = hypot(wp_x - self.position.x, wp_y - self.position.y)

    self.sss.move("base", target)

  def execute(self):
    try:
      self.init_wait()
      self.greet_wait()
      self.inspect_a()
      self.inspect_b()
      self.inspect_c()
      self.inspect_d()
      self.prepare("standby", "standby")
    except (AssertionError, StoppedException, InsufficientInformationException):
      self.signal = ""
      if not self.experiment_id is None:
        rospy.logwarn("Aborting experiment id '%s', condition '%s'", self.experiment_id, self.condition)
      else:
        rospy.logwarn("No experiment active, ignoring abort.")

if __name__ == "__main__":
  rospy.init_node("safety_security_experiment_flow")
  script = ExperimentControl()
  while not rospy.is_shutdown():
    script.execute()

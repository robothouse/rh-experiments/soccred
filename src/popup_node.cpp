#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QVariant>
#include <QDebug>
#include <QQuickItem>
#include <QQuickWindow>
#include <QObject>
#include <QQmlProperty>
#include "popup.h"

#include <initializer_list>
#include <signal.h>
#include <unistd.h>


void catchUnixSignals(std::initializer_list<int> quitSignals) {
    auto handler = [](int sig) -> void {
        // blocking and not aysnc-signal-safe func are valid
        printf("\nquit the application by signal(%d).\n", sig);
        QCoreApplication::quit();
    };

    sigset_t blocking_mask;
    sigemptyset(&blocking_mask);
    for (auto sig : quitSignals)
        sigaddset(&blocking_mask, sig);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask    = blocking_mask;
    sa.sa_flags   = 0;

    for (auto sig : quitSignals)
        sigaction(sig, &sa, nullptr);
}

int main(int argc, char *argv[])
{

  ros::init(argc, argv, "safety_security_popup");
  ros::AsyncSpinner spinner(2);
  spinner.start();
  ROSPopup ros(argc, argv);

  catchUnixSignals({SIGQUIT, SIGINT, SIGTERM, SIGHUP});

  QGuiApplication app(argc, argv);
  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty("ros", &ros);

  engine.load(QUrl(QStringLiteral("qrc:/popup.qml")));
  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}

#include "popup.h"
#include <QDebug>

ROSPopup::ROSPopup(int argc, char **argv, QObject *parent):
QObject(parent),
pub_topic({"/safety_security/cmd"}),
sub_topic({"/safety_security/notify"})
{
  publisher = nh.advertise<std_msgs::String>(pub_topic, 1000);
  subscriber = nh.subscribe(sub_topic, 1000,
                            &ROSPopup::notification_changed,
                            this);
  ROS_INFO("Subscribed to '%s'", sub_topic.c_str());
}

void ROSPopup::notification_changed(const safety_security::PopupMsg::ConstPtr& msg)
{
  ROS_INFO("Received '%s' on '%s'", msg->message.c_str(), sub_topic.c_str());
  setMsg(msg->message.c_str());
  setTitle(msg->title.c_str());
  setNote(msg->note.c_str());
  setLabel(msg->label.c_str());
  setVisible(msg->visible);
  if(!msg->visible){
    publish_string(msg->message);
  }
}

void ROSPopup::publish_string(std::string msg)
{
  std_msgs::String ros_msg;
  ros_msg.data = msg;

  ROS_INFO("Publishing '%s' to '%s'", ros_msg.data.c_str(), pub_topic.c_str());
  publisher.publish(ros_msg);
}

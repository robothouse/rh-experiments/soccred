import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

Window {
  id: root
  width: Screen.width
  height: Screen.height
  title: "Care-O-Bot safety controller"
  visible: ros.gui_visible
  x: Screen.width / 2 - width / 2
  y: Screen.height / 2 - height / 2
  flags: Qt.Window | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint
  color: "#000000"

  GridLayout {
    id: mygrid
    Layout.fillWidth: true
    anchors.fill: parent
    anchors.margins: 20
    rowSpacing: 10
    columns: 1
    rows: 4

    Text {
      anchors.horizontalCenter: parent.horizontalCenter
      id: header
      font.pointSize: 38
      text: ros.title
      color: "#00FFFF"
    }

    Text {
      anchors.horizontalCenter: parent.horizontalCenter
      id: text
      font.pointSize: 32
      text: ros.message
      wrapMode: Text.Wrap
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      Layout.preferredWidth: root.width
      color: "#FFFFFF"
    }
    Text {
      anchors.horizontalCenter: parent.horizontalCenter
      id: hint
      font.pointSize: 20
      text: ros.note
      wrapMode: Text.Wrap
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      Layout.preferredWidth: root.width
      color: "#999999"
    }
    Button {
      anchors.horizontalCenter: parent.horizontalCenter
      id: parameters_button
      text: ros.label
      onClicked: {
        ros.dismiss()
        ros.gui_visible = false
      }
      style: ButtonStyle {
        label: Text {
          font.pointSize: 28
          text: control.text
//          color: "#FFFFFF"
        }
      }
    }
  }
}

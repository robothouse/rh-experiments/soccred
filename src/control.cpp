#include "control.h"
#include <QDebug>
#include <XmlRpcValue.h>

#include <errno.h>
#include <unistd.h>

ROSControl::ROSControl(int argc, char **argv, QObject *parent):
QObject(parent),
say_cob("/sound/say", true),
notify_topic({"/safety_security/notify"}),
cmd_topic({"/safety_security/cmd"}),
rec_service({"/record/cmd"}),
exp_id_param({"/safety_security/experiment_id"}),
exp_trial_list({"/safety_security/experiment_trials"}),
exp_condition_param({"/safety_security/experiment_condition"}),
utterance_param({"/script_server/utterances/"})
{
  empty_list.append("<empty>");
  notify_publisher = nh.advertise<safety_security::PopupMsg>(notify_topic, 1000);
  cmd_publisher = nh.advertise<std_msgs::String>(cmd_topic, 1000);
  rec_client = nh.serviceClient<record_ros::String_cmd>(rec_service);
  cmd_subscriber = nh.subscribe(cmd_topic, 1000,
                            &ROSControl::cmd_received,
                            this);

  XmlRpc::XmlRpcValue filters;
  bool params_loaded = nh.getParam(utterance_param, filters);
  if (params_loaded)
  {

    ROS_ASSERT(filters.getType() == XmlRpc::XmlRpcValue::TypeStruct);
    QStringList new_list;
    for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = filters.begin(); it != filters.end(); ++it) {
      new_list.append(QString::fromStdString((std::string)(it->first)));
    }
    setMsgList(new_list);
  }
  else
  {
    ROS_WARN("[CTRLIF] Utterance parameters could not be loaded from '%s', using an empty list for now.", utterance_param.c_str());
    setMsgList(empty_list);
  }

  setSoundEnabled(true);
  setRunning(false);
}

void ROSControl::cmd_received(const std_msgs::String::ConstPtr& msg)
{
  std::string cmd = msg->data;
  if(cmd.find("experimenter") == std::string::npos)
  {
    if(isRunning() && (cmd == "stop/script" || cmd == "abort/script"))
    {
      std::string external_id, external_condition;
      nh.getParam(exp_id_param, external_id);
      nh.getParam(exp_condition_param, external_condition);

      ROS_INFO("[CTRLIF] Current experiment script ended with status '%s' (id '%s' and condition '%s')",
          cmd.c_str(),
          external_id.c_str(),
          external_condition.c_str());

      record_ros::String_cmd rec_msg;
      rec_msg.request.cmd = "stop";
      ROS_INFO("[CTRLIF] Stopping bag recording. Calling '%s' at '%s'", rec_msg.request.cmd.c_str(), rec_service.c_str());
      rec_client.call(rec_msg);


      std::string trial_data_file = get_dump_file(false);
      ROS_INFO("[CTRLIF] Saving trial parameters. Calling rosparam dump to '%s'", trial_data_file.c_str());
      system(("rosparam dump " +  trial_data_file + " /safety_security").c_str());

      setRunning(false);
    }
  }
}

void ROSControl::notify_msg(const QString msg, QString title, QString note, QString label)
{
  std::string exp_condition, utt_id, param, sentence, int_param, int_sentence;
  utt_id = msg.toStdString();
  nh.getParam(exp_condition_param, exp_condition);
  param = utterance_param + "/" + utt_id + "/" + exp_condition;
  nh.getParam(param, sentence);

  int_param = utterance_param + "/interrupt/" + exp_condition;
  nh.getParam(int_param, int_sentence);

  safety_security::PopupMsg ros_msg;
  ros_msg.message = sentence;
  ros_msg.title = title.toStdString();
  ros_msg.note = note.toStdString();
  ros_msg.label = label.toStdString();
  ros_msg.visible = true;

  if(isSoundEnabled())
  {
    if (utt_id == "oven" ||
        utt_id == "plugs" ||
        utt_id == "plugs_2" ||
        utt_id == "pepper"
    )
    {
//       ROS_INFO("[CTRLIF] Verbalise interrupt utterance. Triggering say action (cob) with '%s'", int_sentence.c_str());
//       cob_sound::SayGoal goal;
//       goal.text = int_sentence;
//       say_cob.sendGoal(goal);

      std::string int_file = get_sound_file("/interrupt/" + exp_condition);
      ROS_INFO("[CTRLIF] Playing interrupt utterance file. Triggering (fetch) sound client with '%s'", int_file.c_str());
      say_fetch.playWave(int_file.c_str());
      usleep(2000000);

//       ROS_INFO("[CTRLIF] Verbalise interrupt utterance. Triggering (fetch) sound client with '%s'", int_sentence.c_str());
//       say_fetch.say(int_sentence.c_str());
//       usleep(2000000);
    }
  }

  ROS_INFO("[CTRLIF] Showing display notification. Publishing '%s' to '%s'", ros_msg.message.c_str(),  notify_topic.c_str());
  notify_publisher.publish(ros_msg);

  if(isSoundEnabled())
  {
//     ROS_INFO("[CTRLIF] Verbalise utterance. Triggering say action (cob) with '%s'", sentence.c_str());
//     cob_sound::SayGoal goal;
//     goal.text = sentence;
//     say_cob.sendGoal(goal);

    std::string snd_file = get_sound_file(utt_id + "/" + exp_condition);
    ROS_INFO("[CTRLIF] Playing utterance file. Triggering (fetch) sound client with '%s'", snd_file.c_str());
    say_fetch.playWave(snd_file.c_str());
    usleep(2000000);

//     ROS_INFO("[CTRLIF] Verbalise utterance. Triggering (fetch) sound client with '%s'", sentence.c_str());
//     say_fetch.say(sentence.c_str());
  }
}

void ROSControl::acted_msg()
{
  std_msgs::String dismiss_msg;
  dismiss_msg.data = "acted/experimenter";

  ROS_INFO("[CTRLIF] Sending dismiss command. Publishing '%s' to '%s'", dismiss_msg.data.c_str(), cmd_topic.c_str());
  cmd_publisher.publish(dismiss_msg);

  safety_security::PopupMsg ros_msg;
  ros_msg.message = "acted/experimenter";
  ros_msg.visible = false;

  ROS_INFO("[CTRLIF] Hiding display notification. Publishing '%s' to '%s'", ros_msg.message.c_str(), notify_topic.c_str());
  notify_publisher.publish(ros_msg);
}

void ROSControl::ignored_msg()
{
  std_msgs::String dismiss_msg;
  dismiss_msg.data = "ignored/experimenter";

  ROS_INFO("[CTRLIF] Sending dismiss command. Publishing '%s' to '%s'", dismiss_msg.data.c_str(), cmd_topic.c_str());
  cmd_publisher.publish(dismiss_msg);

  safety_security::PopupMsg ros_msg;
  ros_msg.message = "ignored/experimenter";
  ros_msg.visible = false;

  ROS_INFO("[CTRLIF] Hiding display notification. Publishing '%s' to '%s'", ros_msg.message.c_str(), notify_topic.c_str());
  notify_publisher.publish(ros_msg);
}

void ROSControl::start_exp(const QString experiment_id, const QString experiment_condition)
{
  std::string exp_id = experiment_id.toStdString();
  std::string exp_condition = experiment_condition.toStdString();

  if(nh.hasParam(exp_trial_list + "/" + exp_id))
  {
    std::map<std::string,std::string> exp_info;
    nh.getParam(exp_trial_list + "/" + exp_id, exp_info);

    ROS_ERROR("[CTRLIF] NOT triggering already existing experiment with ID '%s'! Condition: %s, last state: %s",
              exp_id.c_str(),
              exp_info["condition"].c_str(),
              exp_info["state"].c_str());

    setRunning(false);
    return;
  }

  std::string exp_id_now;
  nh.getParam(exp_id_param, exp_id_now);
  if(exp_id == exp_id_now)
  {
    std::string exp_condition_now;
    ROS_ERROR("[CTRLIF] NOT triggering already initialized/running experiment with ID '%s'! Condition: %s",
              exp_id_now.c_str(),
              exp_condition_now.c_str());
    setRunning(false);
    return;
  }

  XmlRpc::XmlRpcValue filters;
  bool params_loaded = nh.getParam(utterance_param, filters);
  if (params_loaded)
  {
    ROS_ASSERT(filters.getType() == XmlRpc::XmlRpcValue::TypeStruct);
    QStringList new_list;
    for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = filters.begin(); it != filters.end(); ++it) {
      new_list.append(QString::fromStdString((std::string)(it->first)));
    }
    setMsgList(new_list);

    ROS_INFO("[CTRLIF] Initializing experiment (setting id '%s' and condition '%s')",
          exp_id.c_str(),
          exp_condition.c_str());

    nh.setParam(exp_id_param, exp_id);
    nh.setParam(exp_condition_param, exp_condition);

    std::string trial_data_file = get_dump_file(false);
    ROS_INFO("[CTRLIF] Saving trial parameters. Executing 'rosparam dump %s /safety_security'", trial_data_file.c_str());
    system(("rosparam dump " +  trial_data_file + " /safety_security").c_str());

    std::string yaml_file = get_dump_file(true);
    ROS_INFO("[CTRLIF] Saving all ROS parameters. Executing 'rosparam dump %s'", yaml_file.c_str());
    system(("rosparam dump " +  yaml_file).c_str());

    record_ros::String_cmd rec_msg;
    rec_msg.request.cmd = "record";
    ROS_INFO("[CTRLIF] Starting bag recording. Calling '%s' at '%s'", rec_msg.request.cmd.c_str(), rec_service.c_str());
    rec_client.call(rec_msg);

    std_msgs::String init_msg;
    init_msg.data = "init/experimenter";
    ROS_INFO("[CTRLIF] Sending trigger command. Publishing '%s' to '%s'", init_msg.data.c_str(), cmd_topic.c_str());
    cmd_publisher.publish(init_msg);
  }
  else
  {
    ROS_ERROR("[CTRLIF] Experiment parameters not loaded, aborting. Failed to get param '%s'", utterance_param.c_str());
  }
  setRunning(params_loaded);
}

void ROSControl::stop_exp(const QString experiment_id, const QString experiment_condition)
{
  std::string external_id, local_id, external_condition, local_condition;
  nh.getParam(exp_id_param, external_id);
  nh.getParam(exp_condition_param, external_condition);
  local_id = experiment_id.toStdString();
  local_condition = experiment_condition.toStdString();


  ROS_INFO("[CTRLIF] Stopping current experiment (assuming id '%s' and condition '%s')",
           local_id.c_str(),
           local_condition.c_str());

  if(local_id != external_id)
  {
    ROS_WARN("[CTRLIF] Stored experiment ID does not match current value '%s' != '%s'", local_id.c_str(), external_id.c_str());
  }

  if(local_condition != external_condition)
  {
    ROS_WARN("[CTRLIF] Stored experiment condition does not match current value '%s' != '%s'", local_condition.c_str(), external_condition.c_str());
  }

  std_msgs::String stop_msg;
  stop_msg.data = "stop/experimenter";
  ROS_INFO("[CTRLIF] Sending shutdown command. Publishing '%s' to '%s'", stop_msg.data.c_str(), cmd_topic.c_str());
  cmd_publisher.publish(stop_msg);


  record_ros::String_cmd rec_msg;
  rec_msg.request.cmd = "stop";
  ROS_INFO("[CTRLIF] Stopping bag recording. Calling '%s' at '%s'", rec_msg.request.cmd.c_str(), rec_service.c_str());
  rec_client.call(rec_msg);


  std::string trial_data_file = get_dump_file(false);
  ROS_INFO("[CTRLIF] Saving trial parameters. Executing 'rosparam dump %s /safety_security'", trial_data_file.c_str());
  system(("rosparam dump " +  trial_data_file + " /safety_security").c_str());

  setRunning(false);
}

std::string ROSControl::get_sound_file(std::string name) {

  ros::NodeHandle ph("~");
  std::string sound_path, suffix(".ogg");
  ph.param<std::string>("sound_path", sound_path, "/tmp");
  return sound_path + "/" + name + suffix;
}


std::string ROSControl::get_dump_file(bool unique) {

  ros::NodeHandle ph("~");
  std::string param_loc, prefix("params"), suffix(".yaml");
  ph.param<std::string>("dump_params", param_loc, ".");

  if(access(param_loc.c_str(), W_OK) != 0)
  {
    ROS_WARN("Folder '%s' not writable. Falling back to '%s'.",
             param_loc.c_str(),
             "/tmp");
    param_loc = "/tmp";
  }

  ros::WallTime ros_t = ros::WallTime::now();
  char buf[1024] = "";
  time_t t = ros_t.sec;
  struct tm* tms = localtime(&t);
  strftime(buf, 1024, "%Y-%m-%d-%H-%M-%S", tms);

  if(unique)
  {
    return param_loc + "/" + prefix + "_" + std::string(buf) + suffix;
  }
  else
  {
    return param_loc + "/params_all" + suffix;
  }
}


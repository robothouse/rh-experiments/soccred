import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Window {
  color: palette.window
  id: root
  visible: true
  width: 700
  height: 250
  title: (ros.experiment_running ? "[RUNNING] " : "") + "Experiment interface"

  SystemPalette {
    id: palette
    colorGroup: SystemPalette.Active
  }

  Binding { target: ros; property: "sound"; value: sound_box.checked }

  Label {
    id: id_label
    text: "Experiment ID"
    x: 25
    y: 15
  }

  TextField {
    id: experiment_id
    enabled: !ros.experiment_running
    width: 200
    height: 50
    text: "id"
    x: 25
    y: 55
  }

  Label {
    id: condition_label
    text: "Experiment condition"
    x: 250
    y: 15
  }

  ComboBox {
    id: experiment_condition
    enabled: !ros.experiment_running
    width: 200
    height: 50
    textRole: "key"
    model: ListModel {
      ListElement { key: "According (AN)"; value: "an"}
      ListElement { key: "Violating (VN)"; value: "vn" }
    }
    x: 250
    y: 55
  }

  Button {
    id: experiment_control
    x: 475
    y: 55
    width: 200
    height: 50
    text: ros.experiment_running ? "Stop experiment" : "Start experiment"
    onClicked: {
      if(!ros.experiment_running){
        ros.start_experiment(experiment_id.text, experiment_condition.model.get(experiment_condition.currentIndex).value)
      } else {
        ros.stop_experiment(experiment_id.text, experiment_condition.model.get(experiment_condition.currentIndex).value)
      }
    }
  }

  Label {
    id: user_label
    text: "Send Notification"
    x: 25
    y: 140
  }

  ComboBox {
    enabled: ros.experiment_running
    id: notification
    width: 150
    height: 50
    model: ros.msg_list
    x: 25
    y: 175
  }

  CheckBox {
    enabled: ros.experiment_running
    id: sound_box
    text: "Sound?"
    checked: true
    x: 190
    y: 175
    height: 50
  }

  Button {
    enabled: ros.experiment_running
    id: notify_button
    x: 320
    y: 175
    height: 50
    width: 130
    text: "Notify"
    onClicked: ros.notify(notification.currentText)
  }

  Button {
    id: acted_button
    x: 575
    y: 175
    height: 50
    width: 100
    text: "Acted"
    onClicked: ros.acted()
  }

  Button {
    id: ignored_button
    x: 475
    y: 175
    height: 50
    width: 100
    text: "Ignored"
    onClicked: ros.ignored()
  }
}

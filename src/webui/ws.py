#!/usr/bin/env python

import sys, os


import cherrypy
import mimetypes
import rospy
from std_msgs.msg import String
from uh_core.config import server_config

import qi

class Root(object):
  exposed = True

  def __init__(self):
    self.online = 'pepper_website.html'
    self.offline = 'pepper_offline.html'
    self.index = self.online
    self.topic = '/safety_security/pepper'
    self.pub = rospy.Publisher(self.topic, String, queue_size=1)

  def GET(self, *args, **kwargs):
    if not cherrypy.request.path_info.endswith('/'):
      raise cherrypy.HTTPRedirect(cherrypy.request.path_info + '/')

    path = os.path.join(os.path.dirname(os.path.realpath(__file__)), self.index)
    f = open(path)
    text = f.read()
    f.close()

    cherrypy.response.headers['Content-Type'] = mimetypes.guess_type(path)[0]

    return text


  def POST(self, *args, **kwargs):

    try:
      session = qi.Session()
      session.connect("tcp://pepper1-2:9559")
      motion_service  = session.service("ALMotion")
      awake = motion_service.robotIsWakeUp()

      if awake:
        self.index = self.offline
        data = "RELAX/MOTORS"
      else:
        self.index = self.online
        data = "ACTIVATE/MOTORS"

      rospy.loginfo("Publishing '%s' to '%s'...", data, self.topic)
      msg = String()
      msg.data = data
      self.pub.publish(msg)

      if awake:
        motion_service.rest()
      else:
        motion_service.wakeUp()

    except RuntimeError:
      rospy.logwarn("Can't connect to Naoqi at ip tcp://pepper1-2:9559.\n")

    return self.GET(*args, **kwargs)



conf = {
    'global': {
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 1056,
        'server.thread_pool': 10,
        'server.thread_pool_max': -1,
        'environment': 'embedded'
    },
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
    }
}

if __name__ == "__main__":
  rospy.init_node("pepper_webdisplay", disable_signals=True)
  root = Root()
  cherrypy.quickstart(root, '/', conf)

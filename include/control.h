#ifndef ROSCONTROL_H
#define ROSCONTROL_H

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "safety_security/PopupMsg.h"
#include "record_ros/String_cmd.h"
#include <actionlib/client/simple_action_client.h>
#include <cob_sound/SayAction.h>
#include <sound_play/sound_play.h>

#include <QObject>
#include <QString>
#include <QStringList>

class ROSControl : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool sound READ isSoundEnabled WRITE setSoundEnabled NOTIFY soundChanged)
  Q_PROPERTY(bool experiment_running READ isRunning WRITE setRunning NOTIFY stateChanged)
  Q_PROPERTY(QStringList msg_list READ getMsgList WRITE setMsgList NOTIFY msgListChanged)

signals:
  void soundChanged(bool);
  void stateChanged(bool);
  void msgListChanged(QStringList);

public:
  explicit ROSControl(int argc, char **argv, QObject *parent = 0);

  void setSoundEnabled(bool use_sound){
    sound = use_sound;
    emit soundChanged(use_sound);
  }

  bool isSoundEnabled() const {
    return sound;
  }

  void setRunning(bool isRunning){
    experiment_running = isRunning;
    emit stateChanged(experiment_running);
  }

  bool isRunning() const {
    return experiment_running;
  }

  void setMsgList(QStringList new_msg){
    msg_list = new_msg;
    emit msgListChanged(msg_list);
  }

  QStringList getMsgList() const {
    return msg_list;
  }

public slots:
  void notify(const QString msg) {
    notify_msg(msg);
  }

  void acted() {
    acted_msg();
  }

  void ignored() {
    ignored_msg();
  }

  void start_experiment(const QString exp_id, const QString exp_condition) {
    start_exp(exp_id, exp_condition);
  }

  void stop_experiment(const QString exp_id, const QString exp_condition) {
    stop_exp(exp_id, exp_condition);
  }

private:
  ros::NodeHandle nh;
  ros::Publisher notify_publisher;
  ros::Publisher cmd_publisher;
  ros::ServiceClient rec_client;
  ros::Subscriber cmd_subscriber;
  actionlib::SimpleActionClient<cob_sound::SayAction> say_cob;
  sound_play::SoundClient say_fetch;

  void cmd_received(const std_msgs::String::ConstPtr& msg);
  void notify_msg(const QString msg,
                  const QString title=QString("NOTIFICATION"),
                  const QString note=QString(""),
                  const QString label=QString("Dismiss"));
  void acted_msg();
  void ignored_msg();
  void start_exp(const QString exp_id, const QString exp_condition);
  void stop_exp(const QString exp_id, const QString exp_condition);
  std::string get_sound_file(std::string name);
  std::string get_dump_file(bool unique);
  bool experiment_running;
  bool sound;

  std::string notify_topic;
  std::string cmd_topic;
  std::string rec_service;

  std::string exp_id_param;
  std::string exp_condition_param;
  std::string exp_trial_list;
  std::string utterance_param;
  QStringList msg_list;
  QStringList empty_list;

};

#endif // ROSCONTROL_H

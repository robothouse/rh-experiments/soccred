#ifndef ROSPOPUP_H
#define ROSPOPUP_H

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "safety_security/PopupMsg.h"

#include <QObject>
#include <QString>

class ROSPopup : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool gui_visible READ isVisible WRITE setVisible NOTIFY visibilityChanged)
  Q_PROPERTY(QString message READ getMsg WRITE setMsg NOTIFY msgChanged)
  Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)
  Q_PROPERTY(QString note READ getNote WRITE setNote NOTIFY noteChanged)
  Q_PROPERTY(QString label READ getLabel WRITE setLabel NOTIFY labelChanged)

signals:
  void visibilityChanged(bool);
  void msgChanged(QString);
  void titleChanged(QString);
  void noteChanged(QString);
  void labelChanged(QString);

public:
  explicit ROSPopup(int argc, char **argv, QObject *parent = 0);

  void setVisible(bool isVisible){
    gui_visible = isVisible;
    emit visibilityChanged(gui_visible);
  }

  bool isVisible() const {
    return gui_visible;
  }

  void setMsg(QString new_msg){
    message = new_msg;
    emit msgChanged(message);
  }

  QString getMsg() const {
    return message;
  }

  void setTitle(QString new_title){
    title = new_title;
    emit titleChanged(title);
  }

  QString getTitle() const {
    return title;
  }

  QString getNote() const {
    return note;
  }

  void setNote(QString new_note){
    note = new_note;
    emit noteChanged(note);
  }

  void setLabel(QString new_label){
    label = new_label;
    emit labelChanged(label);
  }

  QString getLabel() const {
    return label;
  }

public slots:
  void dismiss() {
    publish_string("dismiss/user");
  }

private:
  ros::NodeHandle nh;
  ros::Publisher publisher;
  ros::Subscriber subscriber;
  void publish_string(std::string msg);
  void notification_changed(const safety_security::PopupMsg::ConstPtr& msg);
  bool gui_visible = false;
  QString message;
  QString title;
  QString note;
  QString label;

  std::string pub_topic;
  std::string sub_topic;
};

#endif // ROSPOPUP_H
